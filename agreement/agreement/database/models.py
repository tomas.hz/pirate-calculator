import datetime

import sqlalchemy

from . import Base
from .utils import UUID, get_uuid

__all__ = ["Answer"]


class Answer(Base):
	__tablename__ = "answers"

	id = sqlalchemy.Column(
		UUID,
		primary_key=True,
		default=get_uuid
	)

	time = sqlalchemy.Column(
		sqlalchemy.DateTime(timezone=True),
		default=datetime.datetime.now,
		nullable=False
	)

	slug = sqlalchemy.Column(
		sqlalchemy.String(128),
		nullable=False
	)

	value = sqlalchemy.Column(
		sqlalchemy.Float(precision=1),
		nullable=False
	)
