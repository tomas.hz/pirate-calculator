$(window).ready(
	function() {
		$("#obec").select2();
		
		$("#obec").on(
			"select2:select",
			function(event) {
				const data = event.params.data;
				
				if (data.id === "zadna") {
					return;
				}
				
				$("#district-filter-form").submit();
			}
		);
	}
);
