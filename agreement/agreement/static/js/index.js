$("#agreement-form").on(
	"submit",
	function(event) {
		event.preventDefault();
		
		for (answersWrapper of $(".question-answers")) {
			let hasAnswer = false;
			
			for (const child of answersWrapper.children(".question-answer").children("input")) {
				if (child.is(":checked")) {
					hasAnswer = true;
				}
			}
			
			if (!hasAnswer) {
				alert("Prosím, zodpověz všechny otázky.");
				
				return;
			}
		}
		
		$("#agreement-form").submit();
	}
);
