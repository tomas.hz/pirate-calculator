"""Pirate archetype calculator. Made for the 2022 communal elections."""

import enum
import json
import logging
import math
import os
import typing

import flask
import flask_limiter
import sqlalchemy
import sqlalchemy.orm


logging.basicConfig(
	format="[%(asctime)s] %(levelname)s in %(module)s: %(message)s",
	level=getattr(
		logging,
		os.environ.get(
			"LOGGING_LEVEL",
			"DEBUG"
		)
	)
)


def create_app() -> flask.Flask:
	"""Creates a :class:`Flask <flask.Flask>` app with basic settings already
	applied.

	Loads the config from the file located where the ``CONFIG_LOCATION``
	environment variable describes, or ``$current_working_directory/config.json``
	if it's not set.

	Registers all blueprints in :mod:`.views`.
	"""

	app = flask.Flask(__name__)

	with app.app_context():
		app.logger.info("Setting up app")

		app.logger.debug("Loading config file")

		with open(
			os.environ.get(
				"CONFIG_LOCATION",
				os.path.join(
					os.getcwd(),
					"config.json"
				)
			),
			"r",
			encoding="utf-8"
		) as f:
			app.config.update(
				json.loads(f.read())
			)

		app.logger.debug("Creating engine")

		database_url = os.environ["DATABASE_URL"]

		if database_url.startswith("postgres://"):
			database_url = database_url.replace("postgres://", "postgresql://")

		sa_engine = sqlalchemy.create_engine(database_url)

		app.sa_session_class = sqlalchemy.orm.scoped_session(
			sqlalchemy.orm.sessionmaker(
				bind=sa_engine
			)
		)

		app.logger.debug("Loading extensions")

		app.limiter = flask_limiter.Limiter(
			app,
			key_func=lambda: flask.request.headers.get("X-Forwarded-For", flask.request.remote_addr),
			default_limits=["500 per day", "100 per hour"]
		)

		from .views import calculator_blueprint

		for blueprint in (calculator_blueprint,):
			app.logger.debug(
				"Registering blueprint: %s",
				blueprint
			)

			app.register_blueprint(blueprint)

		@app.cli.command("reflect")
		def reflect() -> None:
			"""Reflects database models."""

			from .database import Base

			with app.sa_session_class() as sa_session:
				Base.metadata.create_all(bind=sa_engine)

		@app.cli.command("calculate_results")
		def calculate_results() -> None:
			"""Calculates all people's results."""

			max_score = 0
			question_score_taken = False
			people_results = {}

			for person in flask.current_app.config["PEOPLE"]:
				people_results[person] = {
					"score": 0,
					"percentage": 0
				}
				
				for question in flask.current_app.config["QUESTIONS"].values():
					if not question_score_taken:
						max_score += question["weight"]
					
					people_results[person]["score"] += question["responses"].get(person, 0) * question["weight"]

				if not question_score_taken:
					question_score_taken = True

				people_results[person]["percentage"] = math.ceil(
					people_results[person]["score"]
					/ max_score
					* 100
				)

			sorted_district_people = {}

			for district_slug, district in flask.current_app.config["DISTRICTS"].items():
				sorted_district_people[district_slug] = sorted(
					district["people"],
					key=lambda person: people_results[person]["score"],
					reverse=True
				)

			with open("cached_results.json", "w") as results_file:
				json.dump(
					{
						"max": max_score,
						"people": people_results,
						"sorted_district_people": sorted_district_people
					},
					results_file
				)

		@app.before_request
		def before_request() -> None:
			"""TODO: doc"""

			flask.g.sa_session = flask.current_app.sa_session_class()

		@app.teardown_request
		def teardown_request(
			exception: typing.Union[None, Exception]
		) -> None:
			"""Attempts to commit :attr:`flask.g.sa_session` and rolls it back if
			any exception is raised during the process. The exception is then
			logged.

			:param exception: The exception that occurred in the prior request,
				if there was any.
			"""

			if "sa_session" in flask.g:
				# "Clean" the session
				try:
					flask.g.sa_session.commit()
				except Exception as commit_exception:
					flask.g.sa_session.rollback()

					flask.current_app.logger.error(
						"Exception %s raised during the request teardown session commit: %s",
						commit_exception.__class__.__name__,
						(
							commit_exception
							if hasattr(commit_exception, "__str__")
							else "no details"
						)
					)

				flask.current_app.sa_session_class.remove()

		return app
