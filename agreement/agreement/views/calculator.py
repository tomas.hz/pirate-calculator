import http.client
import json
import math
import typing

import flask
import werkzeug.exceptions

from .. import database


calculator_blueprint = flask.Blueprint(
	"calculator",
	__name__
)

def get_closest_candidates(
	answers: typing.Dict[str, int],
	district: typing.Union[None, str] = None
) -> typing.Dict:
	resulting_candidates = {}
	max_points = 0

	for candidate_slug, candidate in flask.current_app.config["PEOPLE"].items():
		if (
			district is not None
			and candidate["district"] != district
		):
			continue
		
		resulting_candidates[candidate_slug] = {
			"points": 0,
			"percentage": 0
		}

	for question_slug, user_response in answers.items():
		for candidate, response in (
			flask.current_app.config
			["QUESTIONS"]
			[question_slug]
			["responses"]
		).items():
			if candidate not in resulting_candidates:
				continue
			
			resulting_candidates[candidate]["points"] += 1 - abs(response - user_response)

		max_points += 1

	for candidate in resulting_candidates:
		resulting_candidates[candidate]["percentage"] = math.floor(
			resulting_candidates[candidate]["points"]
			/ max_points
			* 100
		)

	with open("cached_results.json") as cached_results_file:
		cached_results = json.load(cached_results_file)

	resulting_candidates = dict(
		sorted(
			resulting_candidates.items(),
			key=lambda candidate: candidate[1]["points"],
			reverse=True
		)
	)

	return resulting_candidates


@calculator_blueprint.route("/", methods=["GET"])
def view_index() -> typing.Tuple[flask.Response, int]:
	return flask.render_template("index.html"), http.client.OK


@calculator_blueprint.route("/vysledek", methods=["GET", "POST"])
@flask.current_app.limiter.limit(
	"10/day",
	exempt_when=lambda: flask.request.method != "POST"
)
def view_result() -> typing.Tuple[flask.Response, int]:
	if flask.request.method == "GET":
		selected_district = None
		selected_district_slug = None

		if "obec" in flask.request.args:
			if (
				flask.request.args["obec"]
				not in flask.current_app.config["DISTRICTS"]
				and flask.request.args["obec"] != "zadna"
			):
				raise werkzeug.exceptions.BadRequest
			elif flask.request.args["obec"] != "zadna":
				selected_district = (
					flask.current_app.config
					["DISTRICTS"]
					[flask.request.args["obec"]]
				)
				selected_district_slug = flask.request.args["obec"]

		if "answers" not in flask.request.cookies:
			raise werkzeug.exceptions.BadRequest

		try:
			decoded_cookie = json.loads(flask.request.cookies["answers"])
		except json.JSONDecodeError:
			raise werkzeug.exceptions.BadRequest

		## Input validation
		if (
			"percentage" not in decoded_cookie
			or not isinstance(decoded_cookie["percentage"], int)
			or decoded_cookie["percentage"] < 0
			or decoded_cookie["percentage"] > 100
			or "answers" not in decoded_cookie
			or not isinstance(decoded_cookie["answers"], dict)
		):
			raise werkzeug.exceptions.BadRequest

		possible_answers = list(flask.current_app.config["QUESTIONS"])

		for answer_slug, answer_value in decoded_cookie["answers"].items():
			if (
				answer_slug not in possible_answers
				or not isinstance(answer_value, (float, int))
				or answer_value not in (
					flask.current_app.config["POINTS"]["neg-full"],
					flask.current_app.config["POINTS"]["neg-half"],
					flask.current_app.config["POINTS"]["neut"],
					flask.current_app.config["POINTS"]["pos-half"],
					flask.current_app.config["POINTS"]["pos-full"]
				)
			):
				raise werkzeug.exceptions.BadRequest

			possible_answers.remove(answer_slug)

		if len(possible_answers) != 0:
			possible_answers.remove(answer_slug)

		return flask.render_template(
			"result.html",
			percentage=math.floor(decoded_cookie["percentage"]),
			selected_district=selected_district,
			selected_district_slug=selected_district_slug,
			closest_candidates=get_closest_candidates(
				decoded_cookie["answers"],
				district=selected_district_slug
			)
		), http.client.OK
	else:
		answer_models = []

		with open("cached_results.json") as cached_results_file:
			cached_results = json.load(cached_results_file)

		max_points = cached_results["max"]
		user_points = 0
		user_answers = {}
		
		for question_slug, question in flask.current_app.config["QUESTIONS"].items():
			has_answer = False

			for form_input, point_value in {
				f"{question_slug}-neg-full": flask.current_app.config["POINTS"]["neg-full"],
				f"{question_slug}-neg-half": flask.current_app.config["POINTS"]["neg-half"],
				f"{question_slug}-neut": flask.current_app.config["POINTS"]["neut"],
				f"{question_slug}-pos-half": flask.current_app.config["POINTS"]["pos-half"],
				f"{question_slug}-pos-full": flask.current_app.config["POINTS"]["pos-full"]
			}.items():
				if flask.request.form.get(form_input) == "on":
					has_answer = True

					user_answers[question_slug] = point_value
					user_points += point_value

					answer_models.append(
						database.Answer(
							slug=question_slug,
							value=point_value
						)
					)

			if not has_answer:
				raise werkzeug.exceptions.BadRequest

		user_percentage = user_points / max_points * 100
		
		flask.g.sa_session.add_all(answer_models)

		response = flask.make_response(
			flask.render_template(
				"result.html",
				percentage=math.floor(user_percentage),
				selected_district=None,
				selected_district_slug=None,
				closest_candidates=get_closest_candidates(user_answers)
			)
		)

		response.set_cookie(
			"answers",
			json.dumps({
				"answers": user_answers,
				"percentage": math.floor(user_percentage)
			})
		)

		return response, http.client.OK


@calculator_blueprint.route("/kandidati", methods=["GET"])
def view_candidates_responses() -> typing.Tuple[flask.Response, int]:
	selected_district = None
	selected_district_slug = None
	cached_results = None

	if "obec" in flask.request.args:
		if (
			flask.request.args["obec"]
			not in flask.current_app.config["DISTRICTS"]
			and flask.request.args["obec"] != "zadna"
		):
			raise werkzeug.exceptions.BadRequest
		elif flask.request.args["obec"] != "zadna":
			selected_district = (
				flask.current_app.config
				["DISTRICTS"]
				[flask.request.args["obec"]]
			)
			selected_district_slug = flask.request.args["obec"]

			with open("cached_results.json", "r") as cached_results_file:
				cached_results = json.load(cached_results_file)

	return flask.render_template(
		"candidates_responses.html",
		selected_district=selected_district,
		selected_district_slug=selected_district_slug,
		cached_results=cached_results
	), http.client.OK


@calculator_blueprint.route("/kandidati/<string:person_slug>")
def view_candidate_opinions(
	person_slug: str
) -> typing.Tuple[flask.Response, int]:
	if person_slug not in flask.current_app.config["PEOPLE"]:
		raise werkzeug.exceptions.NotFound

	with open("cached_results.json", "r") as cached_results_file:
		return flask.render_template(
			"candidate_opinions.html",
			person_slug=person_slug,
			cached_results=json.load(cached_results_file)
		)
