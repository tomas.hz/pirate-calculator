import datetime

import sqlalchemy

from . import Base
from .utils import UUID, get_uuid

__all__ = ["Answer"]


class Answer(Base):
	__tablename__ = "answers"

	id = sqlalchemy.Column(
		UUID,
		primary_key=True,
		default=get_uuid
	)

	time = sqlalchemy.Column(
		sqlalchemy.DateTime(timezone=True),
		default=datetime.datetime.now,
		nullable=False
	)

	primary_archetype = sqlalchemy.Column(
		sqlalchemy.String(64),
		nullable=False
	)

	secondary_archetype = sqlalchemy.Column(
		sqlalchemy.String(64),
		nullable=False
	)

	answers = sqlalchemy.Column(
		sqlalchemy.String(65535),
		nullable=False
	)
