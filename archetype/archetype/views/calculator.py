import binascii
import http.client
import json
import os
import random
import typing

import flask
import werkzeug.exceptions

from .. import database


calculator_blueprint = flask.Blueprint(
	"calculator",
	__name__
)

def path_exists(path: str) -> bool:
	return os.path.exists(
		os.path.abspath(
			os.path.dirname(__file__)
		)
		+ "/"
		+ path
	)


@calculator_blueprint.route("/pypl")
def view_archetypes() -> typing.Tuple[flask.Response, int]:
	return flask.render_template(
		"archetypes.html"
	), http.client.OK


def get_closest_candidates(
	primary_slug: str,
	secondary_slug: str,
	district: typing.Union[str, None] = None,
	excluded_slugs: typing.List[str] = []
) -> typing.Dict:
	closest_candidates = []

	closest_primary_candidates = []
	closest_secondary_candidates = []

	for candidate_slug, candidate in flask.current_app.config["PEOPLE"].items():
		if (
			(
				district is not None
				and district != candidate["district"]
			) or candidate_slug in excluded_slugs
		):
			continue

		candidate["slug"] = candidate_slug
		
		if candidate["primary_archetype"] == primary_slug:
			if candidate["secondary_archetype"] == secondary_slug:
				closest_primary_candidates.insert(0, candidate)
			else:
				closest_primary_candidates.append(candidate)
		elif (
			candidate["primary_archetype"] == secondary_slug
			or candidate["secondary_archetype"] == secondary_slug
			or candidate["secondary_archetype"] == primary_slug
		):
			closest_secondary_candidates.append(candidate)

	closest_candidates = (
		closest_primary_candidates
		+ closest_secondary_candidates
	)[:flask.current_app.config["TOP_CANDIDATE_MAX_COUNT"]]

	leaders = []

	for position, candidate in enumerate(closest_candidates):
		if candidate.get("is_leader", False):
			leaders.append(candidate)
			closest_candidates.pop(position)

	return leaders + closest_candidates


@calculator_blueprint.route(
	"/pypl/<string:primary_slug>/<string:secondary_slug>"
)
def view_archetype(
	primary_slug: str,
	secondary_slug: str
) -> typing.Tuple[flask.Response, int]:
	district = flask.request.cookies.get("district")
	
	if (
		primary_slug not in flask.current_app.config["ARCHETYPES"]
		or secondary_slug not in flask.current_app.config["ARCHETYPES"]
	):
		raise werkzeug.exceptions.NotFound

	closest_district_candidates = (
		[]
		if district is None
		else get_closest_candidates(
			primary_slug,
			secondary_slug,
			district
		)
	)

	closest_candidates = get_closest_candidates(
		primary_slug,
		secondary_slug,
		None,
		excluded_slugs=[
			candidate["slug"]
			for candidate
			in closest_district_candidates
		]
	)

	response = flask.make_response(
		flask.render_template(
			"result_archetype.html",
			primary_slug=primary_slug,
			secondary_slug=secondary_slug,
			primary_archetype=(
				flask.current_app.config
				["ARCHETYPES"]
				[primary_slug]
			),
			secondary_archetype=(
				flask.current_app.config
				["ARCHETYPES"]
				[secondary_slug]
			),
			closest_district_candidates=closest_district_candidates,
			closest_candidates=closest_candidates,
			path_exists=path_exists,
			gender=flask.request.cookies.get("gender", "neutral"),
			district=district
		)
	)

	response.set_cookie(
		"primary_archetype",
		primary_slug
	)
	response.set_cookie(
		"secondary_archetype",
		secondary_slug
	)

	return response, http.client.OK


@calculator_blueprint.route("/vysledky", methods=["GET"])
def view_results() -> typing.Tuple[flask.Response, int]:
	primary_archetype = flask.request.cookies.get("primary_archetype")
	secondary_archetype = flask.request.cookies.get("secondary_archetype")

	if (
		primary_archetype not in flask.current_app.config["ARCHETYPES"]
		or secondary_archetype not in flask.current_app.config["ARCHETYPES"]
	):
		raise werkzeug.exceptions.NotFound

	return view_archetype(
		primary_slug=primary_archetype,
		secondary_slug=secondary_archetype
	)


@calculator_blueprint.route("/", methods=["GET", "POST"])
def index() -> typing.Tuple[flask.Response, int]:
	gender = flask.request.cookies.get("gender", "neutral")

	if flask.request.method == "GET":
		questions = list(
			flask.current_app.config["QUESTIONS"].items()
		)

		random.shuffle(questions)

		questions = dict(questions)

		return flask.render_template(
			"index.html",
			gender=gender,
			questions=questions
		), http.client.OK
	else:
		answers = flask.request.form.get("answers")
		district = flask.request.cookies.get("district")

		if answers is None:
			raise werkzeug.exceptions.BadRequest

		try:
			decoded_answers = json.loads(flask.request.form["answers"])
		except (binascii.Error, ValueError):
			raise werkzeug.exceptions.BadRequest

		if not isinstance(decoded_answers, dict):
			raise werkzeug.exceptions.BadRequest

		pirate_archetypes = {}
		anti_pirate_archetypes = {}
		skipped_response_count = 0


		for question_slug, answer in decoded_answers.items():
			if (
				question_slug not in flask.current_app.config["QUESTIONS"]
				or answer not in ("pos-important", "pos", "neg", "neg-important", "skipped")
			):
				raise werkzeug.exceptions.BadRequest

			question = flask.current_app.config["QUESTIONS"][question_slug]

			if answer in ("pos-important", "pos"):
				point_multiplier = (
					1 if answer == "pos"
					else 2
				)

				if question["is_pirate"]:
					for archetype_slug, point_amount in question["points"].items():
						if archetype_slug not in pirate_archetypes:
							pirate_archetypes[archetype_slug] = 0

						pirate_archetypes[archetype_slug] += point_amount * point_multiplier
				else:
					for archetype_slug, point_amount in question["points"].items():
						if archetype_slug not in anti_pirate_archetypes:
							anti_pirate_archetypes[archetype_slug] = 0

						anti_pirate_archetypes[archetype_slug] += point_amount * point_multiplier
			
			# Simpler to understand comparison, though slightly less efficient.
			# Could use `!= "skipped"` instead
			elif answer in ("neg-important", "neg"):
				point_multiplier = (
					1 if answer == "neg"
					else 2
				)

				if "negative_points" in question:
					for archetype_slug, point_amount in question["negative_points"].items():
						if archetype_slug not in pirate_archetypes:
							pirate_archetypes[archetype_slug] = 0

						pirate_archetypes[archetype_slug] += point_amount * point_multiplier
			else:
				skipped_response_count += 1

		if len(anti_pirate_archetypes) != 0:
			# https://geekflare.com/python-dictionary-sorting/
			# Thanks to Geekflare!
			anti_pirate_archetypes = dict(
				sorted(
					anti_pirate_archetypes.items(),
					key=lambda item:item[1],
					reverse=True
				)
			)

			slug = list(anti_pirate_archetypes.keys())[0]

			if (
				anti_pirate_archetypes[slug]
				>= (
					flask.current_app.config["ANTI_PIRATE_MAX_POTENTIAL_POINTS"]
					* flask.current_app.config["ANTI_PIRATE_TRESHOLD"]
				)
			):
				return flask.render_template(
					"anti_pirate_archetype_result.html",
					archetype=(
						flask.current_app.config
						["ARCHETYPES"]
						[slug]
					),
					archetype_slug=slug,
					gender=gender
				), http.client.OK


		# https://geekflare.com/python-dictionary-sorting/
		# Thanks to Geekflare!
		pirate_archetypes = dict(
			sorted(
				pirate_archetypes.items(),
				key=lambda item:item[1],
				reverse=True
			)
		)
		
		positive_response_share = (
			pirate_archetypes[list(pirate_archetypes)[0]]
			/ flask.current_app.config["TOTAL_MAX_POTENTIAL_POINTS"]
		)

		if positive_response_share < (
			1 - flask.current_app.config["WINNING_ARCHETYPE_TRESHOLD"]
		):
			return flask.render_template(
				"result_no_archetype.html",
				gender=gender,
				too_many_skipped=False  # FIXME: Workaround for now
			), http.client.OK

		slugs = list(pirate_archetypes.keys())
		primary_slug = slugs[0]
		secondary_slug = slugs[1]

		response = flask.make_response(
			flask.redirect(
				flask.url_for("calculator.view_results")
			)
		)

		response.set_cookie(
			"primary_archetype",
			primary_slug
		)

		response.set_cookie(
			"secondary_archetype",
			secondary_slug
		)

		return response
