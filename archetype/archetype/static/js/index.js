var alreadyShownAntiPirateNotice = false;

$(document).ready(
	function() {
		$("#archetype-form-submit").on(
			"click",
			function(event) {
				if (formInputMaxLength !== Object.keys(formInput).length) {
					alert("Prosím, zodpověz všechny otázky.");
					return;
				}
				
				const form = document.createElement("form");
				
				form.method = "POST";
				form.action = "";
				form.style.display = "none";
				
				const answerElement = document.createElement("input");
				answerElement.type = "text";
				answerElement.name = "answers";
				answerElement.value = JSON.stringify(formInput);
				
				form.appendChild(answerElement);
				
				document.body.appendChild(form);
				form.submit();
			}
		);

		$(".gender-selector").on(
			"click",
			function(event) {
				Cookies.set("gender", event.currentTarget.dataset.gender);
				
				$(".gender-selector").removeClass("gender-selector-selected");
				$(event.currentTarget).addClass("gender-selector-selected");
				
				window.location.reload();
			}
		);
		
		// Always have this at 0
		$("#archetype-list").scrollLeft(0);
		
		$("#archetype-list-move-left").on(
			"click",
			function(event) {
				const archetypeList = $("#archetype-list");
				
				const scrollDestination = archetypeList.scrollLeft() - 210;
				
				archetypeList.scrollLeft(scrollDestination);
				$("#archetype-list-move-right").prop("disabled", false);
				
				if (scrollDestination <= 0) {
					$(event.currentTarget).prop("disabled", true);
				} else {
					$(event.currentTarget).prop("disabled", false);
				}
			}
		);
		
		$("#archetype-list-move-right").on(
			"click",
			function(event) {
				const archetypeList = $("#archetype-list");
				
				const scrollDestination = archetypeList.scrollLeft() + 210;
				
				archetypeList.scrollLeft(scrollDestination);
				$("#archetype-list-move-left").prop("disabled", false);
				
				if (
					scrollDestination
					=== (
						archetypeList[0].scrollWidth
						- archetypeList.width()
					)
				) {
					$(event.currentTarget).prop("disabled", true);
				} else {
					$(event.currentTarget).prop("disabled", false);
				}
			}
		);
		
		// Some of this can be here
		$(".archetype-input-button,.skip-question").on(
			"click",
			function(event) {
				const percentage = Math.ceil(100 * Object.keys(formInput).length / formInputMaxLength);
				
				$("#progress").css(
					"width",
					`${percentage}%`
				);
				
				if (percentage === 100) {
					$("#progress").css("border-radius", "0");
				}
			}
		);
		
		$(".skip-question").on(
			"click",
			function(event) {
				document.getElementById(
					event.currentTarget.dataset.nextQuestionId
				).scrollIntoView();
			}
		);
		
		$(".go-back-question").on(
			"click",
			function(event) {
				document.getElementById(
					event.currentTarget.dataset.previousQuestionId
				).scrollIntoView();
			}
		);
	}
);
