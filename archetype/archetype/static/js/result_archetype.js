$(document).ready(
	function() {
		$("#district").select2({
			forceDropdownPosition: "above",
			dropdownParent: $("#district").parent()
		});
		
		$("#district").on(
			"select2:select",
			function (event) {
				const district = event.params.data.id;
				
				if (district === "none") {
					Cookies.remove("district");
				} else {
					Cookies.set("district", district);
				}
				
				window.location.replace("/vysledky");
			}
		);
	}
);
