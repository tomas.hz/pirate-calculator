import json

with open("source.json") as sources:
	sources = json.load(sources)
	
	result = {}
	
	for source in sources:
		for question_slug, question_answer in source.items():
			if question_slug not in result:
				result[question_slug] = {
					"neg-important": 0,
					"neg": 0,
					"skipped": 0,
					"pos": 0,
					"pos-important": 0
				}

			for possible_answer in result[question_slug]:
				if possible_answer == question_answer:
					result[question_slug][question_answer] += 1

	with open("question-distribution.csv", "w") as result_file:
		result_file.write("question,neg-important,neg,skipped,pos,pos-important\n")

		with open("../archetype/config.example.json") as config:
			config = json.load(config)
			
			for question_slug, answers in result.items():
				question_content = (
					config['QUESTIONS']
					.get(question_slug, {'content': question_slug})
					['content']
				)
				
				result_file.write(f"\"{question_content}\",")

				for answer_points in answers.values():
					result_file.write(f"{answer_points},")

				result_file.write("\n")
