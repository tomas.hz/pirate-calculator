`agreement/agreement/static/images/*.svg` - icons by [Font Awesome](https://fontawesome.com/license/free), licensed under CC-BY.

`archetype/archetype/static/images/*.svg` - icons by [Font Awesome](https://fontawesome.com/license/free), licensed under CC-BY.
